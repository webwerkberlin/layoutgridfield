Object.defineProperty( exports, "__esModule", {
    value: true
} );
exports.default = undefined;
const _extends = Object.assign || function ( target ) {
    for ( var i = 1; i < arguments.length; i++ ) {
        var source = arguments[ i ];
        for ( var key in source ) {
            if ( Object.prototype.hasOwnProperty.call( source, key ) ) {
                target[ key ] = source[ key ];
            }
        }
    }
    return target;
};
const _createClass = function () {
    function defineProperties( target, props )
    {
        for ( let i = 0; i < props.length; i++ ) {
            let descriptor = props[ i ];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ( "value" in descriptor ) {
                descriptor.writable = true;
            }
            Object.defineProperty( target, descriptor.key, descriptor );
        }
    }

    return function ( Constructor, protoProps, staticProps ) {
        if ( protoProps ) {
            defineProperties( Constructor.prototype, protoProps );
        }
        if ( staticProps ) {
            defineProperties( Constructor, staticProps );
        }
        return Constructor;
    };
}();

const _react = require( 'react' );

const _react2 = _interopRequireDefault( _react );

const _ObjectField2 = require( 'react-jsonschema-form/lib/components/fields/ObjectField' );

const _ObjectField3 = _interopRequireDefault( _ObjectField2 );

const _utils = require( 'react-jsonschema-form/lib/utils' );

const _reactBootstrap = require( 'react-bootstrap' );

function _interopRequireDefault( obj )
{
    return obj && obj.__esModule ? obj : { default: obj };
}

function _objectWithoutProperties( obj, keys )
{
    let target = {};
    for ( let i in obj ) {
        if ( keys.indexOf( i ) >= 0 ) {
            continue;
        }
        if ( !Object.prototype.hasOwnProperty.call( obj, i ) ) {
            continue;
        }
        target[ i ] = obj[ i ];
    }
    return target;
}

function _classCallCheck( instance, Constructor )
{
    if ( !(instance instanceof Constructor) ) {
        throw new TypeError( "Cannot call a class as a function" );
    }
}

function _possibleConstructorReturn( self, call )
{
    if ( !self ) {
        throw new ReferenceError( "this hasn't been initialised - super() hasn't been called" );
    }
    return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits( subClass, superClass )
{
    if ( typeof superClass !== "function" && superClass !== null ) {
        throw new TypeError( "Super expression must either be null or a function, not " + typeof superClass );
    }
    subClass.prototype = Object.create(
        superClass && superClass.prototype,
        { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }
    );
    if ( superClass ) {
        Object.setPrototypeOf ? Object.setPrototypeOf( subClass, superClass ) : subClass.__proto__ = superClass;
    }
}

const LayoutGridField = function ( _ObjectField ) {
    _inherits( LayoutGridField, _ObjectField );

    function LayoutGridField()
    {
        let _ref;
        const args = [];

        _classCallCheck( this, LayoutGridField );

        for ( let _len = arguments.length, args = Array( _len ), _key = 0; _key < _len; _key++ ) {
            args[ _key ] = arguments[ _key ];
        }
        _ref = LayoutGridField.__proto__ || Object.getPrototypeOf( LayoutGridField );
        return _possibleConstructorReturn(
            this,
            _ref.call.apply( _ref, [this].concat( args ) )
        );
    }

    _createClass( LayoutGridField, [
        {
            key: 'render',
            value: function render() {
                const uiSchema = this.props.uiSchema;
                let layoutGridSchema = this.props.layoutGridSchema;

                if ( !layoutGridSchema ) {
                    layoutGridSchema = uiSchema[ 'ui:layout_grid' ];
                }

                if ( layoutGridSchema[ 'ui:row' ] ) {
                    return this.renderRow( layoutGridSchema );
                } else {
                    if ( layoutGridSchema[ 'ui:col' ] ) {
                        return this.renderCol( layoutGridSchema );
                    } else {
                        return this.renderField( layoutGridSchema );
                    }
                }
            }
        }, {
            key: 'renderRow',
            value: function renderRow( layoutGridSchema ) {
                let key = this.props.idSchema[ '$id' ];
                let rows = layoutGridSchema[ 'ui:row' ];

                let group = layoutGridSchema[ 'ui:group' ];

                if ( group ) {
                    let _props$registry = this.props.registry,
                        fields = _props$registry.fields,
                        formContext = _props$registry.formContext;
                    let TitleField = fields.TitleField;
                    let required = this.props.required;

                    let title = group && typeof group === 'string' ? group : this.props.schema.title;
                    return _react2.default.createElement(
                        'fieldset',
                        { className: 'layout-grid-group' },
                        title ? _react2.default.createElement( TitleField, {
                            title: title,
                            description: this.props.schema.description,
                            required: required,
                            formContext: formContext
                        } ) : null,
                        _react2.default.createElement(
                            'div',
                            { className: 'row', key: key },
                            this.renderChildren( rows )
                        )
                    );
                } else {
                    return _react2.default.createElement(
                        'div',
                        { className: 'row', key: key },
                        this.renderChildren( rows )
                    );
                }
            }
        }, {
            key: 'renderCol',
            value: function renderCol( layoutGridSchema ) {
                let key = this.props.idSchema[ '$id' ];

                let _layoutGridSchema$ui = layoutGridSchema[ 'ui:col' ],
                    children = _layoutGridSchema$ui.children,
                    colProps = _objectWithoutProperties( _layoutGridSchema$ui, ['children'] );

                let group = layoutGridSchema[ 'ui:group' ];
                if ( group ) {
                    let _props$registry2 = this.props.registry,
                        fields = _props$registry2.fields,
                        formContext = _props$registry2.formContext;
                    let TitleField = fields.TitleField;
                    let required = this.props.required;

                    let title = group && typeof group === 'string' ? group : null;

                    return _react2.default.createElement(
                        _reactBootstrap.Col,
                        _extends( {}, colProps, { key: key } ),
                        _react2.default.createElement(
                            'fieldset',
                            { className: 'layout-grid-group' },
                            title ? _react2.default.createElement( TitleField, {
                                title: title,
                                required: required,
                                formContext: formContext
                            } ) : null,
                            this.renderChildren( children )
                        )
                    );
                } else {
                    return _react2.default.createElement(
                        _reactBootstrap.Col,
                        _extends( {}, colProps, { key: key } ),
                        this.renderChildren( children )
                    );
                }
            }
        }, {
            key: 'renderChildren',
            value: function renderChildren( childrenLayoutGridSchema ) {
                const _this2 = this;

                const definitions = this.props.registry.definitions;

                const schema = (0, _utils.retrieveSchema)( this.props.schema, definitions );

                return childrenLayoutGridSchema.map( function ( layoutGridSchema, index ) {
                    return _react2.default.createElement( LayoutGridField, _extends( {}, _this2.props, {
                        key: index,
                        schema: schema,
                        layoutGridSchema: layoutGridSchema
                    } ) );
                } );
            }
        }, {
            key: 'renderField',
            value: function renderField( layoutGridSchema ) {
                const _props = this.props,
                    key = _props.id,
                    uiSchema = _props.uiSchema,
                    errorSchema = _props.errorSchema,
                    idSchema = _props.idSchema,
                    disabled = _props.disabled,
                    readonly = _props.readonly,
                    onBlur = _props.onBlur,
                    onFocus = _props.onFocus,
                    formData = _props.formData;
                const _props$registry3 = this.props.registry,
                    definitions = _props$registry3.definitions,
                    fields = _props$registry3.fields;
                const SchemaField = fields.SchemaField;

                const schema = (0, _utils.retrieveSchema)( this.props.schema, definitions );
                let name = void 0;
                let render = void 0;
                if ( typeof layoutGridSchema === 'string' ) {
                    name = layoutGridSchema;
                } else {
                    name = layoutGridSchema.name;
                    render = layoutGridSchema.render;
                }

                if ( schema.properties[ name ] ) {
                    return _react2.default.createElement( SchemaField, {
                        key: key,
                        name: name,
                        required: this.isRequired( name ),
                        schema: schema.properties[ name ],
                        uiSchema: uiSchema[ name ],
                        errorSchema: errorSchema[ name ],
                        idSchema: idSchema[ name ],
                        formData: formData[ name ],
                        onChange: this.onPropertyChange( name ),
                        onBlur: onBlur,
                        onFocus: onFocus,
                        registry: this.props.registry,
                        disabled: disabled,
                        readonly: readonly
                    } );
                } else {
                    const UIComponent = render || function () {
                        return null;
                    };

                    return _react2.default.createElement( UIComponent, {
                        key: key,
                        name: name,
                        formData: formData,
                        errorSchema: errorSchema,
                        uiSchema: uiSchema,
                        schema: schema,
                        registry: this.props.registry
                    } );
                }
            }
        }
    ] );

    return LayoutGridField;
}( _ObjectField3.default );

exports.default = LayoutGridField;
module.exports = exports[ 'default' ];